#include <ros/ros.h>
#include <std_msgs/Bool.h>

#include <wiringPi.h>

static bool g_laser_state = false;

void laserPointerCb(const std_msgs::BoolConstPtr& msg)
{
  g_laser_state = msg->data;
}

int main (int argc, char** argv)
{
  int wiringPi_pin = 21; // Pin 29 on 40-pin header (GPIOX.BIT4, Export GPIO 101)

  ros::init(argc, argv, "laser_node");

  ros::NodeHandle nh;
  ros::NodeHandle nh_p ("~");

  nh_p.param("wiringPi_pin", wiringPi_pin, 21);
  ROS_ASSERT(wiringPi_pin >= 0);

  ros::Subscriber laser_sub = nh.subscribe<std_msgs::Bool>("laser_on", 1, &laserPointerCb);

  wiringPiSetup();

  bool last_laser_state = g_laser_state;

  digitalWrite(wiringPi_pin, 0);

  ros::Rate loop_rate(30);
  while (ros::ok())
  {

    if (last_laser_state != g_laser_state)
    {
      pinMode(wiringPi_pin, g_laser_state);
      last_laser_state = g_laser_state;
    }

    loop_rate.sleep();
    ros::spinOnce();
  }

  pinMode(wiringPi_pin, INPUT);
}
